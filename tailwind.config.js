module.exports = {
  purge: [],
  theme: {
    screens: {
      'sm': {'min': '320px', 'max': '767px'},
      'md': {'min': '768px', 'max': '1024px'},
      'lg': {'min': '1024px', 'max': '1279px'},
      'xl': {'min': '1280px'},

      zIndex:{
        '10':10,
        '20':20,
        '30':30,
        '40':40,
        '50':50,
      }
    },
    extend: {
      fontFamily: {
        'source-sans-pro': ['Source Sans Pro'],
        'mitr': ['Mitr'],
        'roboto-slab': ['Roboto Slab'],
     },
    },
  },
  variants: {},
  plugins: [
    require('@tailwindcss/custom-forms')
  ],
}

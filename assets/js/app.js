
//show and hide navigation drawer
function showSideNav(){
    var toggleSideNav = document.getElementById('nav-drawer');
    toggleSideNav.style.display = 'block';
}
function hideSideNav(){
  var hideNav = document.getElementById('nav-drawer');
  hideNav.style.display= 'none';
}

// course tabs
function openTab(tabName) {
  var i;
  var x = document.getElementsByClassName("tab");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  document.getElementById(tabName).style.display = "block";
}

//edit course
function editCourse(){
  var editBtn = document.getElementById('edit-course');
  editBtn.style.display = 'block';
}
// close edit course
function closeEdit(){
  var closeBtn = document.getElementById('edit-course');
  closeBtn.style.display = 'none';
}

// lecture tabs
function openLecture(lectureTabs) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  document.getElementById(lectureTabs).style.display = "block";
}



function showContent(){
  var chevdown = document.getElementById('intro');
  var chevupBtn = document.getElementById('chev-up');
  var chevdBtn = document.getElementById('chev-down');
  chevdown.style.display = 'block';
  chevupBtn.style.display = 'block';
  chevdBtn.style.display = 'none';
}

function hideContent(){
  var chevup = document.getElementById('intro');
  var chevdownBtn = document.getElementById('chev-up');
  var chevBtn = document.getElementById('chev-down');
  chevdownBtn.style.display = 'none';
  chevup.style.display = 'none';
  chevBtn.style.display = 'block';
}

function showMsg(){
  var msg = document.getElementById('show-msg');
  msg.style.display = 'block';
}

function closeMsg(){
  var cMsg = document.getElementById('show-msg'); 
  cMsg.style.display = 'none';
}

function openForum(forumName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  document.getElementById(forumName).style.display = "block";
}

//profile tabs////
function openprofile(profileName) {
  var i;
  var x = document.getElementsByClassName("profile-tabs");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  document.getElementById(profileName).style.display = "block";  
}

function toggleModal(modalID){
  document.getElementById(modalID).classList.toggle("hidden");
  document.getElementById(modalID + "-backdrop").classList.toggle("hidden");
  document.getElementById(modalID).classList.toggle("flex");
  document.getElementById(modalID + "-backdrop").classList.toggle("flex");
}



